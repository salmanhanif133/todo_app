import React from 'react';
import './styles/app.css'
function App() {
  return (
    <div className="px-24 bg-gray-100 h-screen">
      <div className="pt-8 sm:w-1/2 w-full mx-auto ">
        <p className="text-2xl text-center text-gray-800 font-bold mb-2">To Do List</p>
        <div className="bg-white px-8 py-2 border border-gray-300 text-center mb-2">
          <p>Hello</p>
        </div>
        <div className="bg-white px-8 py-2 border border-gray-300 text-center mb-2">
          <p>Hello</p>
        </div>
        <div className="bg-white px-8 py-2 border border-gray-300 text-center mb-2">
          <p>Hello</p>
        </div>
        <div className="bg-white px-8 py-2 border border-gray-300 text-center mb-2">
          <p>Hello</p>
        </div>
        <div className="bg-white px-8 py-2 border border-gray-300 text-center mb-2">
          <p>Hello</p>
        </div>
      </div>
    </div>
  );
}

export default App;
